package stockbit.page_object;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import stockbit.android_driver.AndroidDriverInstance;

public class BasePage {
    public AndroidDriver driver() {
        return AndroidDriverInstance.androidDriver;
    }

    public void tap(By element) {
        driver().findElement(element).click();
    }

    public void typeOn(By element, String text) {
        driver().findElement(element).sendKeys(text);
    }

    public void assertIsDisplay(By element) {
        try {
            driver().findElement(element).isDisplayed();
        } catch (NoSuchElementException e) {
            throw new AssertionError(String.format("This element '%s' not found", element));
        }
    }
}
